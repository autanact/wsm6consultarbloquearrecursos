
/**
 * WsM6ConsultarBloquearRecursosServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsM6ConsultarBloquearRecursosServiceSkeleton java skeleton for the axisService
     */
    public class WsM6ConsultarBloquearRecursosServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsM6ConsultarBloquearRecursosServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param fechaSolicitud
                                     * @param direccion
                                     * @param prioridadCRM
                                     * @param tipoSolicitud
                                     * @param ofertaEconomica
                                     * @param detalleRespuesta
                                     * @param factibilidadSolicitud
                                     * @param accesos
                                     * @param notas
                                     * @param atributos
         */
        

                 public co.net.une.www.ncainvm6.M6ConsultarBloquearRecursosType M6ConsultarBloquearRecursos
                  (
                  co.net.une.www.ncainvm6.UTCDate fechaSolicitud,co.net.une.www.ncainvm6.Direcciontype direccion,java.lang.String prioridadCRM,java.lang.String tipoSolicitud,java.lang.String ofertaEconomica,co.net.une.www.ncainvm6.Detallerespuestatype detalleRespuesta,java.lang.String factibilidadSolicitud,co.net.une.www.ncainvm6.Listaaccesostype accesos,co.net.une.www.ncainvm6.Listanotastype notas,co.net.une.www.ncainvm6.Listaatributostype atributos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("fechaSolicitud",fechaSolicitud);params.put("direccion",direccion);params.put("prioridadCRM",prioridadCRM);params.put("tipoSolicitud",tipoSolicitud);params.put("ofertaEconomica",ofertaEconomica);params.put("detalleRespuesta",detalleRespuesta);params.put("factibilidadSolicitud",factibilidadSolicitud);params.put("accesos",accesos);params.put("notas",notas);params.put("atributos",atributos);
		try{
		
			return (co.net.une.www.ncainvm6.M6ConsultarBloquearRecursosType)
			this.makeStructuredRequest(serviceName, "M6ConsultarBloquearRecursos", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    