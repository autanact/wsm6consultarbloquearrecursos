/*
 * Generated by XDoclet - Do not edit!
 */
package co.net.une.www.interfaces;

/**
 * Local home interface for WsM6ConsultarBloquearRecursosService.
 * @xdoclet-generated at ${TODAY}
 * @copyright The XDoclet Team
 * @author XDoclet
 * @version ${version}
 */
public interface WsM6ConsultarBloquearRecursosServiceLocalHome
   extends javax.ejb.EJBLocalHome
{
   public static final String COMP_NAME="java:comp/env/ejb/WsM6ConsultarBloquearRecursosServiceLocal";
   public static final String JNDI_NAME="ejb/WsM6ConsultarBloquearRecursosServiceLocal";

   public co.net.une.www.interfaces.WsM6ConsultarBloquearRecursosServiceLocal create()
      throws javax.ejb.CreateException;

}
