
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.ncainvm6;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "arearedtype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Arearedtype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listarespuestafactibilidadtype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listarespuestafactibilidadtype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "equipotype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Equipotype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listaatributostype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listaatributostype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listaequipostype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listaequipostype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "elementospropuestostype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Elementospropuestostype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "factibilidadaccesostype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Factibilidadaccesostype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "detallerespuestatype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Detallerespuestatype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "atributotype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Atributotype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "M6ConsultarBloquearRecursosType".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.M6ConsultarBloquearRecursosType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listainfraestructuratype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listainfraestructuratype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listanotastype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listanotastype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "infraestructuratype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Infraestructuratype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "accesotype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Accesotype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "listaaccesostype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Listaaccesostype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "notatype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Notatype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "UTCDate".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.UTCDate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "direcciontype".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.Direcciontype.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/ncaInvM6".equals(namespaceURI) &&
                  "boundedString14".equals(typeName)){
                   
                            return  co.net.une.www.ncainvm6.BoundedString14.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    